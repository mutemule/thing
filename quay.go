package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"sort"
	"strings"
	"time"

	"github.com/lus/dgc"
	"github.com/bwmarrin/discordgo"
	"go.uber.org/zap"
)

var quayURL = "https://quay.io/api/v1"
var quayClient = &http.Client{}

var bearerToken string
var namespace string

var repositories []Repository

type Repository struct {
	Description string      `json:"description"`
	IsPublic    bool        `json:"is_public"`
	IsStarred   bool        `json:"is_starred"`
	Kind        string      `json:"kind"`
	Name        string      `json:"name"`
	Namespace   string      `json:"namespace"`
	State       interface{} `json:"state"`
}

type Notification struct {
	Config struct {
		Template string `json:"template"`
		URL      string `json:"url"`
	} `json:"config"`
	Event       string `json:"event"`
	EventConfig struct {
		Level string `json:"level"`
	} `json:"event_config"`
	Method           string `json:"method"`
	NumberOfFailures int64  `json:"number_of_failures"`
	Title            string `json:"title"`
	UUID             string `json:"uuid"`
}

type TagTime time.Time

type Tag struct {
	DockerImageID  string  `json:"docker_image_id"`
	EndTs          int64   `json:"end_ts"`
	Expiration     string  `json:"expiration"`
	ImageID        string  `json:"image_id"`
	IsManifestList bool    `json:"is_manifest_list"`
	LastModified   TagTime `json:"last_modified"`
	ManifestDigest string  `json:"manifest_digest"`
	Name           string  `json:"name"`
	Reversion      bool    `json:"reversion"`
	Size           int64   `json:"size"`
	StartTs        int64   `json:"start_ts"`
}

// Implement Marshaler and Unmarshaler interface
func (tt *TagTime) UnmarshalJSON(b []byte) error {
	s := strings.ReplaceAll(string(b), "\"", "")
	t, err := time.Parse("Mon, 02 Jan 2006 15:04:05 -0700", s)
	if err != nil {
		return err
	}
	*tt = TagTime(t)
	return nil
}

func (tt TagTime) MarshalJSON() ([]byte, error) {
	return json.Marshal(tt.Format(time.RFC3339))
}

// Maybe a Format function for printing your date
func (tt TagTime) Format(s string) string {
	t := time.Time(tt)
	return t.Format(s)
}

func initQuay(accessToken, ns string) {
	namespace = ns
	bearerToken = "Bearer " + accessToken

	updateRepolist()
}

func (r *Repository) Notifications() []Notification {
	zap.S().Debugw("Retrieving notifications",
		"repository", r.Name,
	)

	var notificationsMap map[string]json.RawMessage
	var notifications []Notification

	notificationsEndpoint := fmt.Sprintf("/repository/%s/%s/notification/", namespace, r.Name)
	notificationsList, err := call(notificationsEndpoint, "GET")
	err = json.Unmarshal([]byte(notificationsList), &notificationsMap)
	if err != nil {
		zap.S().Errorw("Failed to parse notifications list",
			"repository", r.Name,
			"err", err,
		)
	}

	err = json.Unmarshal(notificationsMap["notifications"], &notifications)
	if err != nil {
		zap.S().Errorw("Failed to parse notifications",
			"repository", r.Name,
			"err", err,
		)
	} else {
		for _, notif := range notifications {
			zap.S().Debugw("Found notification",
				"repository", r.Name,
				"event", notif.Event,
				"title", notif.Title,
				"method", notif.Method,
				"url", notif.Config.URL,
				"level", notif.EventConfig.Level,
			)
		}
	}

	return notifications
}

func (r *Repository) Tags() []Tag {
	zap.S().Debugw("Retrieving tags",
		"repository", r.Name,
	)

	var tagsMap map[string]json.RawMessage
	var tags []Tag

	tagsEndpoint := fmt.Sprintf("/repository/%s/%s/tag/", namespace, r.Name)
	tagsList, err := call(tagsEndpoint, "GET")

	err = json.Unmarshal([]byte(tagsList), &tagsMap)
	if err != nil {
		zap.S().Errorw("Failed to parse tags",
			"repository", r.Name,
			"err", err,
		)
	}

	// Strictly speaking, the tags endpoint is paginated, but we'll just conveniently ignore that for now
	err = json.Unmarshal(tagsMap["tags"], &tags)
	if err != nil {
		zap.S().Errorw("Failed to parse tags",
			"repository", r.Name,
			"err", err,
		)
	} else {
		for _, tag := range tags {
			zap.S().Debugw("Found tag",
				"repository", r.Name,
				"name", tag.Name,
				"size", tag.Size,
				"date", tag.LastModified,
			)
		}
	}

	return tags
}

func (r *Repository) Summarize() string {
	zap.S().Debugw("Summarizing repository",
		"repository", r.Name,
	)

	n := r.Notifications()
	t := r.Tags()

	last_updated := "1970-01-01 12:00"

	for _, tag := range t {
		if tag.Name == "latest" {
			updated_at := tag.LastModified.Format("2006-01-02")
			if updated_at > last_updated {
				last_updated = updated_at
			}
		}
	}

	summary := last_updated
	summary += fmt.Sprintf("\n%d notifications", len(n))
	// summary += fmt.Sprintf("\n\n%s", r.Description)

	return summary
}

func cleanupQuay() {
	zap.S().Debug("Cleaning up Quay webhooks")
	var toPrune []Notification

	for _, repo := range repositories {
		notifications := repo.Notifications()
		for _, n := range notifications {
			if (n.Method == "webhook") && (strings.HasPrefix(n.Title, "thing :: ")) {
				toPrune = append(toPrune, n)
			}
		}
	}
}

func call(endpoint, method string) ([]byte, error) {
	url := quayURL + endpoint
	req, err := http.NewRequest(method, url, nil)

	req.Header.Add("Authorization", bearerToken)
	req.Header.Add("Accept", "application/json")

	resp, err := quayClient.Do(req)
	if err != nil {
		log.Println("Error on response.\n[ERRO] -", err)
	}

	switch resp.StatusCode {
	case 404:
		noneFound := errors.New("Not found")
		return []byte(""), noneFound
	}

	body, _ := ioutil.ReadAll(resp.Body)
	return body, err

}

func updateRepolist() {
	zap.S().Debug("Retrieving updated list of Quay repositories")
	repositories = getRepositories()

	// Not having a repository description can cause grief with other parts, so make sure we always have somthing set
	for i, repo := range repositories {
		if len(repo.Description) == 0 {
			zap.S().Warnw("Setting non-empty repository description",
				"repoName", repo.Name,
				"repoDescription", repo.Description,
			)
			(&repositories[i]).Description = "No description provided"
		}
	}

	sort.Slice(repositories, func(i, j int) bool { return repositories[i].Name < repositories[j].Name })
}

func getRepositories() []Repository {
	endpoint := fmt.Sprintf("/repository?namespace=%v&public=true&private=true", namespace)
	repoList, _ := call(endpoint, "GET")

	var repomap map[string]json.RawMessage
	err := json.Unmarshal([]byte(repoList), &repomap)
	if err != nil {
		zap.S().Fatal(err)
	}

	err = json.Unmarshal(repomap["repositories"], &repositories)

	return repositories
}

func quayReposListHandler(ctx *dgc.Ctx) {
	statusMessage, err := ctx.Session.ChannelMessageSend(ctx.Event.ChannelID, "Retrieving repositories...")
	updateRepolist()

	repoList := make([]*discordgo.MessageEmbedField, len(repositories))
	for i, repo := range repositories {
		repoList[i] = &discordgo.MessageEmbedField{
			Name:   repo.Name,
			Value:  fmt.Sprintf("%s\n[link](https://quay.io/repository/%s/%s)", repo.Summarize(), namespace, repo.Name),
			Inline: true,
		}
	}

	resp := &discordgo.MessageEmbed{
		Type:        discordgo.EmbedTypeRich,
		Title:       fmt.Sprintf("Quay repositories for %s", namespace),
		Description: fmt.Sprintf("%d repositories found:", len(repositories)),
		URL:         fmt.Sprintf("https://quay.io/organization/%s", namespace),
		Fields:      repoList,
	}

	err = ctx.RespondEmbed(resp)
	if err != nil {
		ctx.Session.ChannelMessageEdit(ctx.Event.ChannelID, statusMessage.ID, "Failed to retrieve list of repositories.")
		zap.S().Error(err)
	} else {
		ctx.Session.ChannelMessageDelete(ctx.Event.ChannelID, statusMessage.ID)
	}
}
