module gitlab.com/mutemule/thing

go 1.17

require (
	github.com/bwmarrin/discordgo v0.23.2
	github.com/lus/dgc v1.1.0
	go.uber.org/zap v1.19.0
	gopkg.in/yaml.v2 v2.4.0
)

require (
	github.com/gorilla/websocket v1.4.2 // indirect
	github.com/karrick/tparse/v2 v2.8.2 // indirect
	github.com/zekroTJA/timedmap v1.4.0 // indirect
	go.uber.org/atomic v1.9.0 // indirect
	go.uber.org/multierr v1.7.0 // indirect
	golang.org/x/crypto v0.0.0-20210817164053-32db794688a5 // indirect
	golang.org/x/sys v0.0.0-20201119102817-f84b799fce68 // indirect
)

replace github.com/gorilla/websocket => github.com/gorilla/websocket v1.4.2

replace golang.org/x/crypto => golang.org/x/crypto v0.0.0-20210317152858-513c2a44f670
