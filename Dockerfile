FROM golang:alpine as builder

ARG version="development"
ENV CGO_ENABLED=0

RUN apk update && \
    apk upgrade && \
    apk add ca-certificates tzdata

# Build the binary
WORKDIR /go/src/thing
COPY . .
RUN go install -ldflags="-X 'main.Version=${version}'"

RUN adduser -H -h / -D -g "thing" thing
RUN update-ca-certificates


FROM scratch

COPY --from=builder /go/bin/thing /thing
# COPY --from=builder /go/src/thing/config.yaml /

COPY --from=builder /usr/share/zoneinfo/ /usr/share/zoneinfo
COPY --from=builder /etc/ssl/certs/ /etc/ssl/certs

ENTRYPOINT ["/thing"]
