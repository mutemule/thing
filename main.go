package main

import (
	"fmt"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/lus/dgc"
	"github.com/bwmarrin/discordgo"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

func initLogger() {
	config := zap.NewProductionConfig()
	config.Level = zap.NewAtomicLevelAt(zapcore.DebugLevel)
	config.EncoderConfig.EncodeLevel = zapcore.CapitalLevelEncoder
	config.EncoderConfig.TimeKey = "timestamp"
	config.EncoderConfig.EncodeTime = zapcore.ISO8601TimeEncoder

	logger, _ := config.Build()
	zap.ReplaceGlobals(logger)
	defer logger.Sync()

	zap.S().Debug("Logging initalized")
}

func main() {
	initLogger()

	var config Config
	err := config.Parse("config.yaml")
	if err != nil {
		zap.S().Fatal(err)
	}

	//zap.S().Debugw("Found configuration",
	//	"data", config,
	//)

	// Authenticate to Discord and initialize our session
	session, err := discordgo.New("Bot " + config.Discord.Token)
	if err != nil {
		zap.S().Fatal(err)
	}

	err = session.Open()
	if err != nil {
		zap.S().Fatal(err)
	}

	// Dump something here to indicate that we have authenticated, and maybe who we are
	zap.S().Infow("Connected to Discord",
		"os", session.Identify.Properties.OS,
		"browser", session.Identify.Properties.Browser,
		"device", session.Identify.Properties.Device,
	)

	guilds, err := session.UserGuilds(100, "", "")
	if err != nil {
		zap.S().Error("Failed to retrieve the list of connected guilds.")
		zap.S().Fatal(err)
	}

	for _, guild := range guilds {
		zap.S().Infow("Connected to guild",
			"guildID", guild.ID,
			"guildName", guild.Name,
		)
	}

	defer func() {
		sc := make(chan os.Signal, 1)
		signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
		<-sc
		cleanupQuay()
	}()

	// GetRepos(config.Quay.AccessToken)
	initQuay(config.Quay.AccessToken, config.Quay.Namespace)

	router := dgc.Create(&dgc.Router{
		Prefixes: []string{"!"},

		BotsAllowed: false,
		Commands:    []*dgc.Command{},
		Middlewares: []dgc.Middleware{},
	})

	router.RegisterDefaultHelpCommand(session, nil)

	router.RegisterMiddleware(func(next dgc.ExecutionHandler) dgc.ExecutionHandler {
		return func(ctx *dgc.Ctx) {
			guild, _ := ctx.Session.Guild(ctx.Event.GuildID)
			channel, _ := ctx.Session.Channel(ctx.Event.ChannelID)
			cmd := ctx.Command.Name
			args := ctx.Arguments.Raw()

			zap.S().Debugw("Received command",
				"command", cmd,
				"arguments", args,
				"guildID", guild.ID,
				"guildName", guild.Name,
				"channelID", channel.ID,
				"channelName", channel.Name,
				"authorID", ctx.Event.Author.ID,
				"authorName", ctx.Event.Author.Username,
			)
			next(ctx)
		}
	})

	router.RegisterCmd(&dgc.Command{
		Name:       "ping",
		IgnoreCase: true,

		Description: "Checks bot presence and latency.",
		Usage:       "ping",
		Example:     "ping",

		Flags:       []string{},
		SubCommands: []*dgc.Command{},
		RateLimiter: dgc.NewRateLimiter(1*time.Second, 1*time.Second, func(ctx *dgc.Ctx) {
			ctx.RespondText("You are being rate limited!")
		}),

		Handler: func(ctx *dgc.Ctx) {
			latency := ctx.Session.HeartbeatLatency().Round(time.Millisecond)
			resp := fmt.Sprintf("Pong! (Latency: %v)", latency)
			ctx.RespondText(resp)
		},
	})

	registryReposCmd := &dgc.Command{
		Name:        "repos",
		IgnoreCase:  true,
		Description: "Lists repositories in the container image registry.",
		Usage:       "repos",
		Example:     "registry repos",
		Flags:       []string{},
		SubCommands: []*dgc.Command{},
		Handler:     quayReposListHandler,
	}

	router.RegisterCmd(&dgc.Command{
		Name:       "registry",
		IgnoreCase: true,

		Description: "Interacts with the container image registry.",
		Usage:       "registry",
		Example:     "registry repos",

		Flags:       []string{},
		SubCommands: []*dgc.Command{registryReposCmd},
		RateLimiter: dgc.NewRateLimiter(1*time.Second, 1*time.Second, func(ctx *dgc.Ctx) {
			ctx.RespondText("You are being rate limited!")
		}),

		Handler: quayReposListHandler,
	})

	router.Initialize(session)
}
