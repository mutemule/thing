package main

import (
	"fmt"
	"io/ioutil"
	"os"

	"go.uber.org/zap"
	"gopkg.in/yaml.v2"
)

type Config struct {
	Discord struct {
		Guild int64  `yaml:"guild"`
		Token string `yaml:"token"`
	} `yaml:"discord"`

	Quay struct {
		AccessToken  string   `yaml:"access_token"`
		Namespace    string   `yaml:"namespace"`
		Repositories []string `yaml:"repos"`
	} `yaml:"quay"`
}

func (c *Config) Parse(cfgfile string) error {
	zap.S().Debug("Entering Config.Parse")

	// Read the first configuration file available
	cfgFilePath, err := getConfigFilePath(cfgfile)
	if err != nil {
		return err
	}
	zap.S().Debugw("Found configuration file",
		"location", cfgFilePath,
	)

	cfgData, err := ioutil.ReadFile(cfgFilePath)
	zap.S().Debug("Loaded configuration data")
	if err != nil {
		return err
	}

	return yaml.Unmarshal(cfgData, c)
}

func getConfigFilePath(cfgfile string) (string, error) {
	userConfigDir, _ := os.UserConfigDir()
	userConfigDir += "/thing/"
	systemConfigDir := "/etc/thing"
	containerConfigDir := "/config"
	currentDirectory, _ := os.Getwd()

	configDirs := []string{currentDirectory, userConfigDir, systemConfigDir, containerConfigDir}

	for _, configDir := range configDirs {
		configFilePath := fmt.Sprintf("%s/%s", configDir, cfgfile)
		zap.S().Debugw("Looking for configuration file",
			"location", configFilePath,
		)

		if _, err := os.Stat(configFilePath); err == nil {
			return configFilePath, nil
		}
	}

	err := fmt.Errorf("Failed to find configuration file.")
	// If none of them were found, just return the filename and hope it all works out
	return "", err
}
